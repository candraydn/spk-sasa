@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Pentunjuk Penggunaan Sistem') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>
                        <ol>
                            <li>Pilih Perhitungan -> Bobot Kriteria</li>
                            <li>Memilih kriteria yang lebih penting, kemudian masukkan nilai kepentingan kriteria (1 - 9)</li>
                            <li>Kemudian submit. Jika hasil Consistency Ratio (CR) > 10%, maka ulangi step 3.</li>
                            <li>Selanjutnya Pilih perhitungan -> Bobot Matakuliah</li>
                            <li>Pilih semester</li>
                            <li>Memberi nilai preferensi (1 - 5) antar alternatif pada setiap kriteria. Kemudian klik Submit.</li>
                            <li>Hasil pengurutan mata kuliah dapat dilihat berdasarkan urutan net flow</li>
                            <li>Hasil pengurutan juga dapat dilihat pada tab Perhitungan -> Laporan</li>
                            <li>Pada halaman Laporan, Pilih semester.</li>
                            <li>Hasil pengurutan mata kuliah pada semester tersebut akan muncul</li>
                        </ol>
                    </p>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">{{ __('Daftar Mata Kuliah') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Kuliah</th>
                                <th>SKS</th>
                                <th>Semester</th>
                                <th>Dosen</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $d)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $d->nama_matakuliah }}</td>
                                <td>{{ $d->semester_matakuliah }}</td>
                                <td>{{ $d->semester_matakuliah }}</td>
                                <td>{{ $d->dosen_matakuliah }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
