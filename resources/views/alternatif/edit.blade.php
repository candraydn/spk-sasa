@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Alternatif</div>

                <div class="card-body">
                    <div class="my-4">
                        <form method="post" action="{{route('alternatif.update',$alterData->id)}}">
                            @method('patch')
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control mx-2" name="matkul"
                                placeholder="Nama Mata Kuliah baru" value="{{ $alterData->nama_matakuliah }}" required>
                            </div>
                            <div class="form-group">
                            <select class="custom-select mx-2" name="semester" required>
                                <option value="">Pilih Semester</option>
                                <option value="1" {{ ($alterData->semester_matakuliah == 1) ? 'selected' : ''}}>Semester 1</option>
                                <option value="2" {{ ($alterData->semester_matakuliah == 2) ? 'selected' : ''}}>Semester 2</option>
                                <option value="3" {{ ($alterData->semester_matakuliah == 3) ? 'selected' : ''}}>Semester 3</option>
                                <option value="4" {{ ($alterData->semester_matakuliah == 4) ? 'selected' : ''}}>Semester 4</option>
                                <option value="5" {{ ($alterData->semester_matakuliah == 5) ? 'selected' : ''}}>Semester 5</option>
                                <option value="6" {{ ($alterData->semester_matakuliah == 6) ? 'selected' : ''}}>Semester 6</option>
                                <option value="7" {{ ($alterData->semester_matakuliah == 7) ? 'selected' : ''}}>Semester 7</option>
                                <option value="8" {{ ($alterData->semester_matakuliah == 8) ? 'selected' : ''}}>Semester 8</option>
                            </select>
                            </div>
                            <div class="form-group">
                            <select class="custom-select mx-2" name="sks" required>
                                <option value="">Pilih SKS</option>
                                <option value="1" {{ ($alterData->sks_matakuliah == 1) ? 'selected' : ''}}>1 SKS</option>
                                <option value="2" {{ ($alterData->sks_matakuliah == 2) ? 'selected' : ''}}>2 SKS</option>
                                <option value="3" {{ ($alterData->sks_matakuliah == 3) ? 'selected' : ''}}>3 SKS</option>
                                <option value="4" {{ ($alterData->sks_matakuliah == 4) ? 'selected' : ''}}>4 SKS</option>
                                <option value="5" {{ ($alterData->sks_matakuliah == 5) ? 'selected' : ''}}>5 SKS</option>
                                <option value="6" {{ ($alterData->semester_matakuliah == 6) ? 'selected' : ''}}>6 SKS</option>
                            </select>
                            </div>
                            <div class="form-group">
                            <input type="text" class="form-control mx-2" name="dosen"
                                placeholder="Nama Dosen Pengampu" value="{{ $alterData->dosen_matakuliah }}" required>
                            </div>
                            <div class="form-group">
                            <button class="btn btn-success mx-2" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
